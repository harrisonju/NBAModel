import csv
import urllib2
from bs4 import BeautifulSoup

outfile = open("scrape_stats.csv", "w")
writer = csv.writer(outfile, delimiter=',',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)

player_page = 'https://www.basketball-reference.com/leagues/NBA_2018_per_game.html'
page = urllib2.urlopen(player_page)
tree = BeautifulSoup(page, "lxml")
table_tag = tree.find("table")
list_of_rows=[]
for row in table_tag.findAll('tr'):
  list_of_cells=[]
  for cell in row.findAll(["th", "td"]):
    text = cell.text
    list_of_cells.append(text)
  list_of_rows.append(list_of_cells)

for item in list_of_rows:
  writer.writerow(item)
  print(' '.join(item))



