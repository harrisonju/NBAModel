import csv
import urllib2
from bs4 import BeautifulSoup

import sys
reload(sys)
sys.setdefaultencoding('utf8')

outfile = open("scrape_team_stats.csv", "w")
writer = csv.writer(outfile, delimiter=',',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)

player_page = 'https://www.nbastuffer.com/2017-2018-nba-team-stats/'
page = urllib2.urlopen(player_page)
tree = BeautifulSoup(page, "lxml")
table_div = tree.find('table')
list_of_rows=[]

for row in table_div.findAll('tr'):
  list_of_cells=[]
  for cell in row.findAll(["th", "td"]):
    text = cell.text
    list_of_cells.append(text)
  list_of_rows.append(list_of_cells)

for item in list_of_rows:
  writer.writerow(item)
  print(' '.join(item))



