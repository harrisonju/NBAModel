# NBA Projections Model
Project NBA stats using vegas lines and player metrics for the use in fantasy sports mainly daily fantasy sports.

## Motivation
I started off with fantasy football and it's kind of how I got into fantasy sports. As a winner for a big GPP tournament in FanDuel, now that I'm a better programmer
I wanted to work on something as a side project that's a passion of mine. 

## To Do
1. create scrape_nba_metrics.py to create csv //done
2. create scrape_metrics.py to create csv for advanced stats
3. create a basic script to be able to read from schedule csv and create projections for the games being played
4. create a script that generate player projections for given games
5. create a script to create optimal lineup
6. create script to get team stats

## Ideal Goal
I want to work on this in steps. First goal is to be able to create any type of projections.
Second goal is to be able to pivot easily when a player is injured or out.
Third is to be able to create projections with more criteria that's more than just numbers from the games(ie. consistency, player floor, player ceiling, etc)
Fourth is to be able to implement some sort of data mining/machine learning to create optimal category weighing
Fifth is to be able to host this online for public use with UI whether it be paid or not.
Sixth is to become a millionaire, because who doesn't.

## Design
This hasn't been thought out yet. I want to work on things step by step rather than looking at the big picture

##Getting Started
1. run scrape_stats script to generate stats csv
2. run scrape_metrics script to generate advanced stats csv
3. ...
